# CC6803 is installed in its default location.
CC=/opt/cc68/bin/cc68
AS=/opt/cc68/bin/as68
LD=/opt/cc68/bin/ld68

MAME=~/Developpement/mame/alice # Path to the MAME executable on my computer... Change it to yours while waiting for a better Makefile

CC_FLAGS=-D__TANDY_MC10__ -I /opt/cc68/include/mc10/ -m6803 -c # -S # Uncomment to list
AS_FLAGS=
# Specify Data, BSS will go after DATA if not specified
# Zero page is set at $90, as the first 32 bytes are used by the system
LD_FLAGS=-b -C 4096 -D 13126 -Z 0x90

SYSTEM_LIBS=/opt/cc68/lib/libio6803.a /opt/cc68/lib/lib6803.a /opt/cc68/lib/libmc10.a /opt/cc68/lib/libc.a

# Transformation rules
.s.o:
	$(AS) $(AS_FLAGS) $<

.c.o:
	$(CC) $(CC_FLAGS) $<

# Source file lists
SRC_C=main.c
SRC_S=alice_lib.s

# Object files from both .c and .s files
OBJ=$(SRC_C:.c=.o) $(SRC_S:.s=.o)

# Link everything and create the ROM file (to be run with EXEC 4096)
all: $(OBJ)
	$(LD) $(LD_FLAGS) /opt/cc68/lib/crt0_mc10.o $(OBJ) $(SYSTEM_LIBS) -o main.bin 
	tail --bytes=+4097 main.bin > main.rom

# Link everything and create the tape file (to be loaded by CLOADM then EXEC 17500)
tape: $(OBJ)
	$(CC) -tmc10 $(OBJ) -o main1234

# Launch the ROM in MAME
# Uses a custom build of MAME with the Multiports extension
launch: all
	$(MAME) alice32 -max -ui_active -ext multi -cart main.rom

clean:
	rm *.o
