#include <stdio.h>

extern void set_cursor_home();
extern void clear_screen();
extern void set_screen();
extern void set_40_columns();

int main()
{
    set_screen();
    set_40_columns();
    clear_screen();
    set_cursor_home();
    puts("OK, THIS WORKS!");
    return 0;
}

