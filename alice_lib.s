    .setcpu 6803
    .code

	; Inspired by 6502man's shared code
DEVNU   .equ $00E8		; 0 = ecran  1 = imprimante	
CURSOR	.equ $3280		; LDD = x,y du curseur
OUTCA	.equ $F9C6		; LDAA = code ascii
OUTTX	.equ $E7A8		; LDX = adresse chaine (terminé par null)
CLS		.equ $FBD4		; efface ecran
TXCOLOR	.equ $FB		; couleur fond et ecriture des caractères (40 colonnes)

    .export _set_screen
    .export _set_40_columns
    .export _clear_screen
    .export _set_cursor_home

_set_screen:
	clr     DEVNU
    rts

_set_40_columns:
	ldaa    #$01
	staa    12314
	jsr     54316
	ldaa    #$30
	staa    TXCOLOR
    rts

_clear_screen:
	jmp     CLS

_set_cursor_home:
	ldd #$0100
	std CURSOR
    rts
