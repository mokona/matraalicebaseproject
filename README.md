# Base projet for Alice computers

This is a base project using the [CC6303 compiler](https://github.com/EtchedPixels/CC6303).

It is not using the current -tmc10 options of the compiler, which is creating a tape.
Rather, it creates a ROM which resides in $1000 (for the Multiports extension).


## Notes

### Assembly usage

Example usage of the assembler: `/opt/cc68/bin/as68 input.s` 

Usage of `.org` in the source assembly file seems to mess up with the linker. Not sure why yet. Anyway, base
addresses are passed to the linker.

Example usage of the linker: `/opt/cc68/bin/ld68  -b -C 4096 input.o -m input.map -o input.out`

* `-b` is for absolute address linkage.
* `-C` gives the start address for `.code` section

The linker will fill the missing parts from address $0000 with $00. So the output file needs to be cut to obtain the ROM file (which is also done by the `tapify` tool to create a tape file)

Cut the file: `tail --bytes=+4097 input.out > input.rom`

Check the result with the mame disassembler for example: `unidasm input.rom -arch m6803`

### C usage

Example usage of the C compiler frontend: `/opt/cc68/bin/cc68 -D__TANDY_MC10__ -I /opt/cc68/include/mc10/ -c -S main.c`

Note that the frontend also knows what to do with .s, .S and .o files.


### Linker usage for Matra Alice

Example usage of the linker:

```
/opt/cc68/bin/ld68 -b -C 4096 -B 12288 /opt/cc68/lib/crt0_mc10.o main.o
    /opt/cc68/lib/libio6803.a /opt/cc68/lib/lib6803.a /opt/cc68/lib/libmc10.a /opt/cc68/lib/libc.a
```

See the linker usage above in the Assembly usage section. Here, the usage is to create a ROM file
that can be used with the Multiports Extension.

The read-only section is located at $1000. The BSS section must be in a RAM section (here, 12288).

As a reminder, for Alice/MC-10

* System RAM is in between $4000 and $4FFF
* Extended RAM is between $5000 and $BFFF

